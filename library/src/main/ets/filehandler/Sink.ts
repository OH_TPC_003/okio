/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { fileIo as fs } from '@kit.CoreFileKit';
import Log from '../log.js';

export default class Sink {
    filePath: string = ''

    constructor(filePath) {
        this.filePath = filePath;
    }

    write(data, isAppend) {
        if (isAppend) {
            this.writeData(data);
        } else {
            if (this.exist()) this.deleteFile();
            this.writeFile(data);
        }
    }

    writeFile(content) {
        try {
            let fd = fs.openSync(this.filePath, fs.OpenMode.READ_WRITE | fs.OpenMode.CREATE);
            fs.truncateSync(fd.path)
            fs.writeSync(fd.fd, content)
            fs.fsyncSync(fd.fd)
            fs.closeSync(fd)
        } catch (e) {
            Log.showError("Sink write Error -  " + e);
        }
    }

    writeData(content) {
        try {
            let fd = fs.openSync(this.filePath, fs.OpenMode.READ_WRITE | fs.OpenMode.CREATE);
            let stat = fs.statSync(this.filePath)
            fs.writeSync(fd.fd, content, {
                length: stat.size
            })
            fs.closeSync(fd)
        } catch (e) {
            Log.showError("Sink writeData Error -  " + e);
        }
    }

    deleteFile() {
        fs.unlinkSync(this.filePath);
    }

    exist() {
        try {
            let stat = fs.statSync(this.filePath)
            return stat.isFile()
        } catch (e) {
            return false
        }
    }
}